let path=require("path");//引入node中的path模块
let baseConfig=require("./base.config");
let {merge}=require("webpack-merge");
module.exports=merge(baseConfig,{
    devServer:{
      static: {
        directory: path.join(__dirname, 'dist'),//静态资源路径
      },
      compress: true,//压缩
      port: 8080,//端口号
      open:true//自动在浏览器中打开
    }
})

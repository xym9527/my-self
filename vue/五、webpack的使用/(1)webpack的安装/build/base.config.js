let path=require("path");//引入node中的path模块
const {VueLoaderPlugin} = require('vue-loader');//VueLoaderPlugin是vue-loader的伴生创建，不需要单独安装，直接引入vue-loader就行了
const htmlWebpackPlugin=require("html-webpack-plugin");
module.exports={
    // 入口：可以是字符串、数组、对象，这里入口只有一个，所以写一个字符串即可
    entry:["./src/main.js"],
    // 出口：通常是一个对象，里面至少包含两个重要属性，path和filename
    output:{},
    // 配置loader
    module: {
        rules: [
          {
            test: /\.css$/i,//正则表达式，表示匹配所有的css文件
            use: [ 'style-loader','css-loader'],
          },
          {
            test: /\.s[ac]ss$/i,
            use: [
              // 将 JS 字符串生成为 style 节点
              'style-loader',
              // 将 CSS 转化成 CommonJS 模块
              'css-loader',
              // 将 Sass 编译成 CSS
              'sass-loader',
            ],
          },
          {
            test:/\.vue$/,
            use:['vue-loader']
          }
        ],
      },
    resolve:{
      // alias:别名
      alias:{
        "vue$":'vue/dist/vue.esm.js'
      }
    },
    plugins:[
      new VueLoaderPlugin(),
      new htmlWebpackPlugin({
        template:"index.html"
      })
    ],
    
}
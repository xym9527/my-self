// 1.使用commonJs模块化规范
let math=require("./js/mathUtils");

console.log("1234");
console.log(math.add(3,4));
console.log(math.sub(3,4));

// 2.使用es6的模块化规范
import * as obj from "./js/one";
console.log(obj);

// 3.把css文件当做模块引入，不需要用变量接受，因为后续不会使用
// 只有把css引入到入口文件，webpack打包的时候，根据入口文件查找依赖，才会找到css文件，才会打包css文件
require("./css/normal.css");

// 4.把scss文件引入
require("./scss/qwe.scss");



// 5.引入vue模块
import Vue from "vue";
// import mycpn from "./js/app.js";//引入组件模块
import App from "./vue/App.vue";

let app=new Vue({
    el:"#app",
    template:`<my-cpn></my-cpn>`,//使用组件
    // 注册组件
    components:{
        "my-cpn":App
    }
})
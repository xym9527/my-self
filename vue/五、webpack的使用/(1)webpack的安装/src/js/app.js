// 把组件封装成模块
export default{
    template:`
    <div>
        <h1>{{message}}</h1>
        <button @click="btn">按钮</button>
    </div>
    `,
    data(){
        return {
            message:"hello world"
        }
    },
    methods:{
        btn(){
            this.message=666666
        }
    }
}
const {
  defineConfig
} = require('@vue/cli-service')

// 路径依赖
const path = require('path')
const { config } = require('process')

// 查找文件方法
const resolve = dir => {
  return path.join(__dirname, dir)
}


module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: './',
  // 方法一：基础的配置方式
  // configureWebpack: {
  //   resolve:{
  //     alias:{
  //       '@':resolve('src'),
  //       'components':resolve('src/components'),
  //       'assets':resolve('src/assets')

  //     }
  //   }
    
  // }

  // 方法二：webpack4版本的webpack-chain写法
  chainWebpack:config=>{
    config.resolve.alias
    .set('@',resolve("src"))
    .set('components',resolve('src/components'))
    .set('assets',resolve('src/assets'))
  }
})
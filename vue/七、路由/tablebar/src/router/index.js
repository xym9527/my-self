import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// 路由懒加载
let Home=()=>import("@/views/Home/Home");
let News=()=>import("./../views/News/News");
let Shop=()=>import("./../views/Shop/Shop");
let Mine=()=>import("./../views/Mine/Mine");

// 配置组件和路径的映射关系
const routes = [
  {
    path:"/",
    redirect:"/home"
  },
  {
    path:"/home",
    component:Home
  },
  {
    path:"/news",
    component:News
  },
  {
    path:"/shop",
    component:Shop
  },
  {
    path:"/mine",
    component:Mine
  },
]

const router = new VueRouter({
  routes,
  // mode:"history"
})

export default router

// 缓存原来的 push 方法
const originalPush = VueRouter.prototype.push
//指定新的push方法
VueRouter.prototype.push = function (location, onResolve, onReject) {
  // location:表示传入的路由地址
  // onResolve：表示resolve函数
  // onReject：表示reject函数
  //制定了一个成功或者失败的回调
    if (onResolve || onReject) {
        //直接调用原来的 push 方法
        // originalPush(location, onResolve, onReject) 
        //这里使用 call 可以直接指定当前的 router 对象，要是没有这个就会默认是window 对象，
        //启用严格模式之后就是 undefined
        return originalPush.call(this, location, onResolve, onReject)
    }
    //如果没有指明一个成功或者失败的回调，需要使用到catch处理
    return originalPush.call(this, location).catch((err) => {
        //如果是重复导航所引起的错误的话
        if (VueRouter.isNavigationFailure(err)) {
            //这个 return err 返回的是成功的 Promise，其中Promise的value是 err
            return err
        }
        //如果是其他的错误就会接着往下传
        return Promise.reject(err)
    })
}


//缓存原来的replace 方法
const originalReplace = VueRouter.prototype.replace
//指定了一个新的 replace 方法
VueRouter.prototype.replace = function (location, onResolve, onReject) {
    if (onResolve || onReject) {
        return originalReplace.call(this, location, onResolve, onReject)
    }
    return originalReplace.call(this, location).catch((err) => {
        if (VueRouter.isNavigationFailure(err)) {
            return err
        }
        return Promise.reject(err)
    })
}

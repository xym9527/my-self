// 1.引入项目模块
import Vue from 'vue'
import VueRouter from 'vue-router'

// 2.通过Vue.use方法安装VueRouter插件
Vue.use(VueRouter)

// 引入组件
// import Home from "../components/Home";
// import About from "../components/About";
// import User from "../components/User";

// 动态加载组件
let Home=()=>import("./../components/Home");
let About=()=>import("./../components/About");
let User=()=>import("./../components/User");

let Child1=()=>import("./../components/HomeChild1");
let Child2=()=>import("./../components/HomeChild2");

// import Child1 from "./../components/HomeChild1";
// import Child2 from "./../components/HomeChild2";



// 单独抽离出来写
const routes = [
  // 默认展示home组件
  {
    path:"/",//根路径
    redirect:"/home" 
    // redirect是重定向, 也就是我们将根路径重定向到/home的路径下
  },
  {
    path: "/home",
    component:Home,
    // 原数据
    meta:{
      title:"首页",
      keepAlive:true
    },
    children:[
      {
        path:"/",//根路径
        redirect:"child1" 
        // redirect是重定向
      },
      {
        name:"child1",
        path:"child1",
        // 要注意，以 / 开头的嵌套路径会被当作根路径。 
        // 这让你充分的使用嵌套组件而无须设置嵌套的路径。
        component:Child1,
        meta:{
          keepAlive:true
        },

      },
      {
        name:"child2",
        path:"child2",
        component:Child2,
        meta:{
          keepAlive:true
        },

      }
    ]
  },
  {
    path: "/about",
    component:About,
    // 原数据
    meta:{
      title:"关于"
    },
  },
  {
    path:"/user/:userId",
    component:User,
    // 原数据
    meta:{
      title:"用户"
    },
  }

]

// 3.实例化VueRouter对象
const router = new VueRouter({
  // 配置路由和组件之间的关系
  routes,
  // mode:"history",
  linkActiveClass:"active"
})

// 4.将router对象导出，然后导入到Vue实例化对象中
export default router


router.beforeEach((to,from,next)=>{
  // console.log(to);
  document.title=to.matched[0].meta.title;
  next();
})

// 缓存原来的 push 方法
const originalPush = VueRouter.prototype.push
//指定新的push方法
VueRouter.prototype.push = function (location, onResolve, onReject) {
  // location:表示传入的路由地址
  // onResolve：表示resolve函数
  // onReject：表示reject函数
  //制定了一个成功或者失败的回调
    if (onResolve || onReject) {
        //直接调用原来的 push 方法
        // originalPush(location, onResolve, onReject)  
        //这里使用 call 可以直接指定当前的 router 对象，要是没有这个就会默认是window 对象，启用严格模式之后就是 undefined
        return originalPush.call(this, location, onResolve, onReject)
    }
    //如果没有指明一个成功或者失败的回调，需要使用到catch处理
    return originalPush.call(this, location).catch((err) => {
        //如果是重复导航所引起的错误的话
        if (VueRouter.isNavigationFailure(err)) {
            //这个 return err 返回的是成功的 Promise，其中Promise的value是 err
            return err
        }
        //如果是其他的错误就会接着往下传
        return Promise.reject(err)
    })
}

//缓存原来的replace 方法
const originalReplace = VueRouter.prototype.replace
//指定了一个新的 replace 方法
VueRouter.prototype.replace = function (location, onResolve, onReject) {
    if (onResolve || onReject) {
        return originalReplace.call(this, location, onResolve, onReject)
    }
    return originalReplace.call(this, location).catch((err) => {
        if (VueRouter.isNavigationFailure(err)) {
            return err
        }
        return Promise.reject(err)
    })
}


// router.onError((error) => {
//   const pattern = /Loading chunk (\d)+ failed/g;
//   const isChunkLoadFailed = error.message.match(pattern);
//   if(isChunkLoadFailed){
//     // 用路由的replace方法，并没有相当于F5刷新页面，失败的js文件并没有从新请求，
//     // 会导致一直尝试replace页面导致死循环，而用 location.reload 方法，
//     // 相当于触发F5刷新页面，虽然用户体验上来说会有刷新加载察觉，
//     // 但不会导致页面卡死及死循环，从而曲线救国解决该问题
//       location.reload();
//       // const targetPath = $router.history.pending.fullPath;
//       // $router.replace(targetPath);
//   }
  
// });


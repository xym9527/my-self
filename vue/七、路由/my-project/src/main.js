import Vue from 'vue'
import App from './App.vue'

// 5.导入router模块
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,//6.使用组件
  render: h => h(App)
}).$mount('#app')

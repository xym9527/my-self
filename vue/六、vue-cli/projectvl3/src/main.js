import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
// 产品的构建信息，在开发过程中可以改为true,就能够看到很多提示信息
// 一般用处不大，所以为false

new Vue({
  render: h => h(App),
}).$mount('#app')

// 相当于：
// new Vue({
//   el:"#app",//如果内部有el属性，vue源码就会进行判断，然后执行mount钩子函数，挂载
//   render:h=>h(App)
// })

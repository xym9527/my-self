// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

let mycpn={
  template:`<div>{{message}}</div>`,
  data(){
    return {
      message:"hello world"
    }
  }
}

new Vue({
  el: '#app',
  router,
  // components: { App },
  // template: '<App/>'
  render: function (createElement) {
    // createElement:这是一个回调函数，主要用于创建元素，创建出来的元素会直接替代掉id=app的元素
    // createElement("标签名"，{属性}，[内容1，内容2])
    // 1.普通写法：
    // return createElement("ul",
    // {class:"list"},
    // ["列表标题",createElement("li",["西瓜"])]);

    // 2.插入组件
    // return createElement(mycpn);

    // 3.插入组件：App也是组件
    return createElement(App);
  }
})

import * as TYPE from "./mutation-types"
export default {
    updateName(state, playload) {
      state.person.name = playload;
    },
    // 第一种：方法中包含一个参数state
    [TYPE.ADD](state) {
      state.count++;
    },
    [TYPE.SUB](state) {
      state.count--;
    },
    // 第二种：方法中包含两个参数
    // 第一个是state
    // 第二个是提交载荷（Playload），是外部通过 store.commit 方法触发 mutations 时额外带入的值
    [TYPE.ADDCOUNTNUM](state, playload) {
      console.log(playload);
      // 1.普通提交风格中：playload=num
      // state.count+=playload;
      // 2.特殊提交风格中：playload={num:5,age:19}
      state.count += playload.num;
    },
    changeName(state, playload) {
      // 已经存在于store中的属性可以修改，是响应式的
      state.person.name=playload;
      // 不存在于store中的属性可以修改，但是不响应式的
      // 可以在浏览器中通过devTools插件看到详细信息
      // state.person.friends=playload;

      // 要做到不存在的属性也是响应式的，需要特定的方法

      // 数组中通过数组下标操作数据增删改查，数据不是响应式的
      // state.students[1]={id:4,name:"小米",age:33};//改
      // state.students[4]={id:4,name:"小王",age:33};//增
      // delete state.students[1];//删

      // 数组中是响应式方法的有：push\unshift\pop\shift\splice
      // state.students.push({id:4,name:"小米",age:33});
      // state.students.unshift({id:4,name:"小米",age:33});
      // state.students.pop();
      // state.students.shift();
      // state.students.splice(1,1);

      // 对象中通过点访问法或者中括号访问法增加属性，数据不是响应式的
      // 对象中通过delete删除属性，数据不是响应式的
      // delete state.person.age;

      // Vue.set方法：可以同时用于对象和数组的增加/修改数据的方法，数据是响应式的
      // Vue.set(state.person,"friends","小红");//对象增加
      // Vue.set(state.person,"name","小王");//对象修改
      // Vue.set(state.students,4,{id:4,name:"小米",age:33});//数组增加
      // Vue.set(state.students,1,{id:1,name:"小李",age:88});//数组修改

      // Vue.delete方法：可以同时用于对象和数组的删除数据的方法，数据是响应式的
      // Vue.delete(state.person, "age"); //对象删除
      // Vue.delete(state.students, 1); //数组删除


    }

  }
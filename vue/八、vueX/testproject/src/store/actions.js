export default {
    // context:上下文对象，context=store
    act_updateName(context, playload) {
      return new Promise(function (resolve, reject) {
        setTimeout(function () {
          // 必须是在actions中的方法里面触发mutations里面的方法，
          // 这样devTools插件才能监听数据的变化
          context.commit("updateName", playload);
          resolve("异步操作已完成！");
        }, 5000);
      })
    }
  }
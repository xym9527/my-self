export default {
    // 需求一：找出年龄大于20岁的学生 
    getStudentsArr(state) {
      return state.students.filter(item => {
        if (item.age > 20) {
          return true
        }
      })
    },
    // 需求二：找出年龄大于20岁的学生的数量
    getStudentsNum(state, getters) {
      return getters.getStudentsArr.length
    },
    // 需求三：找出大于指定年龄的学生
    getStuArr(state) {
      return function (age) {
        return state.students.filter(item => {
          if (item.age > age) {
            return true
          }
        })
      }

    }
  }
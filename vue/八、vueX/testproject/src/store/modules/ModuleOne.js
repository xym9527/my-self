export default {
    state:{
      userName:"张三"
    },
    mutations:{
      changeName2(state, playload) {
        state.userName=playload
      }
    },
    actions:{
      // context:上下文对象，在这里context=ModuleOne,
      // context还包含根对象（store）的内容
      act_changeName(context,playload){
        console.log(context);
        setTimeout(()=>{
          context.commit("changeName2",playload);
        },5000);
      }
    },
    getters:{
      fullName(state){
        return state.userName+"111";
      },
      fullName2(state,getters,rootState){
        // state:当前Module里面的state对象
        // getters：当前Module里面的getters对象
        // rootState：根store对象里面的state对象
        return state.userName+getters.fullName+rootState.count;
      }
    }
  }
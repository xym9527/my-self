// 1.引入相关插件
import Vue from 'vue'
import Vuex from 'vuex'

import mutations from './mutations'
import getters from './getters'
import actions from './actions'
import ModuleOne from './modules/ModuleOne'
import ModuleTwo from './modules/ModuleTwo'

// 2.安装VueX
Vue.use(Vuex)

let state = {
  count: 10,
  students: [{
      id: 1,
      name: "xyz",
      age: 18
    },
    {
      id: 2,
      name: "abc",
      age: 28
    },
    {
      id: 3,
      name: "qwe",
      age: 20
    },
    {
      id: 4,
      name: "asd",
      age: 21
    },
  ],
  person: {
    name: "小米",
    age: 18,
    height: 180
  }
}
// 3.实例化对象
export default new Vuex.Store({
  modules: {
    one: ModuleOne,
    two: ModuleTwo
  },
  state,
  getters,
  mutations,
  actions,

})
// 4.挂载到入口文件main.js中
// 1.在main.js文件中无法直接访问变量a,因为每个变量都有自己的作用域，变量a的作用域是one.js
// console.log(a);//报错

// 2.那我们该怎么去访问？
// 因为Node里面帮我们实现了CommonJS的规范，CommonJS想要访问不同模块之间的变量该怎么去访问？
// 是不是得把对应的标识符导出去？

// 3.在one.js里面添加exports

// 4.在main.js里面使用require导入one.js

// let obj=require("./1.one");//使用变量obj接受导过来的数据对象
// console.log(obj);//{ a: 123, b: 456 }

// 总结：上面这行完成了什么操作呢?理解下面这句话，Node中的模块化一目了然
// (1)意味着main中的obj变量等于exports对象;
// (2)也就是require通过各种查找方式，最终找到了exports这个对象;
// (3)并且将这个exports对象赋值给了obj变量;
// (4)obj变量就是exports对象了;
// =>本质就是引用赋值，require和exports指向的对象是同一个对象。

// 5.案例证明本质就是引用赋值
// =>one.js里面的代码：
// let name = "bar"
 
// exports.name = name
 
// setTimeout(() => {
//   // name = "why"
//   exports.name = "why"
// }, 2000)

// =>main.js里面的代码：
// 探讨require的本质
// const bar = require("./1.one.js")
// console.log(bar.name)
 
// // 4s之后重新获取name
// setTimeout(() => {
//   console.log(bar.name)
// }, 4000)

// 六.exports导出、module.exports导出
// 1.但是Node中我们经常导出东西的时候，又是通过module.exports导出的:
// module.exports和exports有什么关系或者区别呢?

// 2.我们追根溯源，通过维基百科中对CommonJS规范的解析:
// CommonJS中是没有module.exports的概念的;
// 但是为了实现模块的导出，Node中使用的是Module的类，每一个模块都是Module的一个实例，也就是module
// 所以在Node中真正用于导出的其实根本不是exports，而是module.exports;
// 因为module才是导出的真正实现者;

// 3.但是，为什么exports也可以导出呢?
// 这是因为module对象的exports属性是exports对象的一个引用;
// 也就是说module.exports = exports = main中的obj;

// 举例：
// 第一步：one.js里面使用module.exports导出数据
// module.exports={
//     a:123,
//     b:456,
//     fn(){
//         console.log(6666);
//     }
// }
// 第二步：main.js使用require引入one.js文件
// let obj=require("./1.one");
// console.log(obj);//{ a: 123, b: 456, fn: [Function: fn] }

// 七、require工作步骤
// 我们现在已经知道，require是一个函数，可以帮助我们引入一个文件（模块）中导出的对象。
// 那么，require的查找规则是怎么样的呢?
// 导入语法：require(X)

// 情况一:X是一个Node核心模块，比如path、http
// 直接返回核心模块，并且停止查找

// 情况二:X是以/或../或/(根目录)开头的

// 第一步:将X当做一个文件在对应的目录下查找;
// 1.如果有后缀名，按照后缀名的格式查找对应的文件
// 2.如果没有后缀名，会按照如下顺序:
// => 1>直接查找文件X
// => 2>查找X.js文件√
// => 3>查找X.json文件
// => 4>查找X.node文件
// 举例：
// let obj=require("./1.one");
// console.log(obj);

// 第二步:没有找到对应的文件，将X作为一个目录
// 查找目录下面的index文件
//  1>查找X/index.js文件
//  2>查找X/index.json文件
//  3>查找X/index.node文件
// 如果没有找到，那么报错:not found
// 举例:
// let obj=require("./test");
// console.log(obj);

// 情况三:直接是一个×（没有路径)，并且X不是一个核心模块
// 举例：
// let mysql=require("mysql");
// console.log(mysql);

// 八、模块的加载过程
// 1.模块在被第一次引入时，模块中的js代码会被运行一次

// 2.模块被多次引入时,会缓存，最终只加载（运行)一次
// 为什么只会加载运行一次呢?
// 这是因为每个模块对象module都有一个属性:loaded。
// 为false表示还没有加载，为true表示已经加载;

// 3.如果有循环引入，那么加载顺序是什么?
// 如果出现右图(案例.png)模块的引用关系，那么加载顺序是什么呢?
// 这个其实是一种数据结构:图结构;
// 图结构在遍历的过程中，有深度优先搜索〈DFS, depth first search）和广度优先搜索（BFS, breadth first search)
// Node采用的是深度优先算法: main -> aaa -> ccc -> ddd -> eee ->bbb

// 概念解析：https://www.zhihu.com/tardis/bd/art/74472146?source_id=1001
// 1.图结构
// 2.深度优先搜索
// 3.广度优先搜索

// 九.CommonJS规范缺点
// 1.CommonJS加载模块是同步的:
// 同步的意味着只有等到对应的模块加载完毕，当前模块中的内容才能被运行;
// 这个在服务器不会有什么问题，因为服务器加载的js文件都是本地文件，加载速度非常快;
// 2.如果将它应用于浏览器呢?
// 浏览器加载js文件需要先从服务器将文件下载下来，之后再加载运行;
// 那么采用同步的就意味着后续的js代码都无法正常运行，即使是一些简单的DOM操作;
// 3.所以在浏览器中，我们通常不使用CommonJS规范:
// 当然在webpack中使用CommonJS是另外一回事;
// 因为它会将我们的代码转成浏览器可以直接执行的代码;
// 4.在早期为了可以在浏览器中使用模块化，通常会采用AMD或CMD:
// 但是目前一方面现代的浏览器已经支持ES Modules，
// 另一方面借助于webpack等工具可以实现对CommonJS或者ESModule代码的转换;
// AMD和CMD已经使用非常少了;

// 十、AMD规范和CMD规范
// 1.AMD主要是应用于浏览器的一种模块化规范:
// AMD是Asynchronous Module Definition(异步模块定义）的缩写;
// 它采用的是异步加载模块;
// 事实上AMD的规范还要早于CommonJS，但是CommonJS目前依然在被使用，而AMD使用的较少了;

// 2.CMD规范也是应用于浏览器的一种模块化规范:
// CMD是Common Module Definition（通用模块定义）的缩写;
// 它也采用的也是异步加载模块，但是它将CommonJS的优点吸收了过来;
// 但是目前CMD使用也非常少了;

// 十一、ES Module(ES6的模块化实现)
// JavaScript没有模块化一直是它的痛点，所以才会产生我们前面学习的社区规范:CommonJS、AMD、CMD等，
// 所以在ECMA推出自己的模块化系统时，大家也是兴奋异常。
// 1.ES Module和CommonJS的模块化有一些不同之处:
//    一方面它使用了import和export关键字;
//    另一方面它采用编译期的静态分析，并且也加入了动态引用的方式;

// 2.ES Module模块采用export和import关键字来实现模块化:
//    export负责将模块内的内容导出;
//    import负责从其他模块导入内容;

// 3.了解:采用ES Module将自动采用严格模式: use strict

// 举例：
// 第一步：创建html文件，引入两个模块，代码如下：
// <!-- 注意：一定要加type="module"属性，把整个js文件转成模块，否则就还是普通js文件-->
//     <script src="./1.one.js" type="module"></script>
//     <script src="./2.main.js" type="module"></script>

// 第二步：把one.js里面的数据导出，代码如下：
// let a=123;
// let b=456;
// let c=789;
// export {a,b,c};

//第三步：在main.js里面导入数据，代码如下： 
// import {a,b,c} from "./1.one.js";//一定要加.js后缀
// console.log(a,b,c);

// 注意：html文件在浏览器需要以启服务的方式打开
// 导入数据的a,b,c变量名必须与导出数据的变量名一致，后面会详细介绍

// 十二、export关键字
// export关键字将一个模块中的变量、函数、类等导出;
// 我们希望将其他中内容全部导出，它可以有如下的方式:

// 方式一:在语句声明的前面直接加上export关键字
// 举例：export let a=123;

// 方式二:将所有需要导出的标识符，放到export后面的{}中
// 注意:这里的{}里面不是ES6的对象字面量的增强写法，{}也不是表示一个对象的;
// 所以: export {name: name}，是错误的写法;
// 举例：
// let a=123;
// let b=456;
// let c=789;
// export {a,b,c};

// 方式三:导出时给标识符起一个别名
// 通过as关键字起别名
// 举例：
// var a = 1;
// var b = 'fun';
// function fun() {}
// export {
//     a as a1,
//     b as b1,
//     fun as fun1
// }

// 十三、import关键字
// import关键字负责从另外一个模块中导入内容
// 导入内容的方式也有多种:
// 方式一: import{标识符列表} from '模块';
//     注意:这里的{}也不是一个对象，里面只是存放导入的标识符列表内容;
//          括号内的变量名必须与模块中导出的变量名相同。
//          同时，引入的变量是只读的，不可修改。
// 举例：import {a,b,c} from "./one"

// 方式二:导入时给标识符起别名
//     通过as关键字起别名
// 举例：import { a as a1 } from "./one.js";

// 方式三:通过*将模块功能放到一个模块功能对象(a module object)上
// =>通过*可以导入模块中所有的export变量
// 举例：import * as obj from "./one.js"; 

// 十四、default用法（默认导出）
// 1.前面我们学习的导出功能都是有名字的导出(named exports) :
// 在导出export时指定了名字;
// 在导入import时需要知道具体的名字;

// 2.还有一种导出叫做默认导出( default export)
// 默认导出export时可以不需要指定名字;
// 在导入时不需要使用{}，并且可以自己来指定名字;
// 它也方便我们和现有的CommonJS等规范相互操作;

// 注意:在一个模块中，只能有一个默认导出( default export) ;

// 举例：
// 导出：
// export default function(){
//     console.log("6666")
// }
// 导入：myname是任意变量名
// import myname from "./1.one";

// 十五、import函数
// 1.通过import加载一个模块，是不可以在其放到逻辑代码中的
// 举例:语法错误
// if(a>10){
//     import {a,b,c} from "./1.one"
// }

// 2.为什么会出现这个情况呢?
// 这是因为ES Module在被JS引擎解析时，就必须知道它的依赖关系;
// 由于这个时候js代码没有任何的运行，所以无法在进行类似于if判断中根据代码的执行情况;
// 甚至拼接路径的写法也是错误的:因为我们必须到运行时能确定path的值;

// 3.但是某些情况下，我们确确实实希望动态的来加载某一个模块:
// 如果根据不同的条件，动态来选择加载模块的路径;
// 这个时候我们需要使用import()函数来动态加载;
// import函数返回一个Promise，可以通过then获取结果;

// 举例：
// let flag=true;
// if(flag){
//     import("./1.one").then(function(res){
//         console.log(res);
//     })
// }else{
//     import("./1.two").then(function(res){
//         console.log(res);
//     })
// }

// 十六、ES Module的解析过程（了解）
// ES Module的解析过程可以划分为三个阶段:
// 1.阶段一:构建(Construction)，根据地址查找js文件，并且下载，将其解析成模块记录（Module Record) ;
// 2.阶段二:实例化(Instantiation)，对模块记录进行实例化，并且分配内存空间，解析模块的导入和导出语句，把模块指向对应的内存地址。
// 3.阶段三:运行（Evaluation)，运行代码，计算值，并且将值填充到内存地址中;
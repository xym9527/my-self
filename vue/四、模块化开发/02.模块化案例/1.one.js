// 第一阶段：讲解步骤
// let a=123;
// let b=456;

// 注意：exports是一个对象，可以添加多个属性，这个对象添加的属性都会导出去
// exports.a=a;
// exports.b=b;


// 第二阶段：案例
// let name = "bar"

// exports.name = name

// setTimeout(() => {
//   // name = "why"
//   exports.name = "why"
// }, 2000)

// 第三阶段：使用module.exports导出数据
// module.exports={
//     a:123,
//     b:456,
//     fn(){
//         console.log(6666);
//     }
// }

// 第四阶段：
let a=123;
let b=456;
let c=789;
export {a,b,c};

// var a = 1;
// var b = 'fun';
// function fun() {}
// export {
//     a as a1,
//     b as b1,
//     fun as fun1

// }

// export default function(){
//     console.log("6666")
// }
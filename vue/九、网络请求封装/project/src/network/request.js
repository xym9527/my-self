import axios from "axios";
export default function request(config){
    // 创建axios实例
    const axiosInstance=axios.create({
        baseURL:"http://localhost:3000",
        timeout:5000
    });

    // axios的拦截器
    // （1）请求拦截器的作用
    // =>修改config中的一些不符合服务器要求的信息
    // =>在页面中展示发送网络请求时，等待的请求的图标
    // =>检测某些网络请求（比如登录（token）），必须携带一些特殊的信息
    axiosInstance.interceptors.request.use(config=>{
        // console.log("xxxx",config);
        // console.log("来到了请求成功的拦截中");
        return config;
        //一定要返回config，否则就意味着发送请求失败
        // 因为在这里把请求拦截下来了
    },err=>{
        // console.log("xxxx",err);
        // console.log("来到了请求失败的拦截中");
        return err;
    })

    // (2)响应拦截器的作用
    // =>响应的成功拦截中，主要是对数据进行过滤
    // =>响应的失败拦截中，可以根据status判断报错的错误码，跳转到不同的错误提示页面。
    axiosInstance.interceptors.response.use(response=>{
        // console.log(response);
        // console.log("来到了响应成功的拦截中");
        return response.data;
        // 对数据进行过滤，因为data才是真正服务器返回的数据，
        // 其他的内容都是axios这个框架自己加的，不需要
    },err=>{
        // console.log(err);
        // console.log("来到了响应失败的拦截中");
        if(err && err.response){
            switch(err.response.status){
                case 400:
                    err.message="请求错误";
                    break;
                case 401:
                    err.message="未授权访问";
                    break;
            }
        
        }
        return err;
    });


    // 把处理返回值的步骤返回出去
    // 方法一：多传入两个回调函数success和err,用于处理返回值
    // axiosInstance(config).then(res=>{
    //     success(res);
    // }).catch(rej=>{
    //     err(rej);
    // })

    // 方法二：直接返回一个promise对象（推荐使用）
    return axiosInstance(config);
}
import "@/App";
// 1.导入路由相关组件
import { BrowserRouter as Router, Link, Routes, Route } from "react-router-dom";

// 导入路由配置对象
import Layout from "./pages/layout";

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route element></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;

// 第一步：创建仓库存储请求到的账单数据列表
import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

const billListSlice = createSlice({
  name: 'billList',
  initialState: {
    value: []
  },
// 同步修改数据方法
  reducers: {
    setBillList: (state,actions) => {
      state.value =actions.payload
    }
  }
})

// 解构同步方法
const { setBillList } = billListSlice.actions

// 异步方法
const getBillList=()=>{
    // 异步请求
    return async (dispatch)=>{
        // let res=await axios.get("http://geek.itheima.net/v1_0/channels");
        let res=[
          {
              "type":"pay",
              "money":-99,
              "date":"2022-10-24 10:36:42",
              "useFor":"drinks",
              "id":1
          },
          {
              "type":"pay",
              "money":-88,
              "date":"2022-10-24 10:12:40",
              "useFor":"longdistance",
              "id":2
          },
          {
              "type":"income",
              "money":100,
              "date":"2022-10-22 12:00:00",
              "useFor":"bonus",
              "id":3
          },
          {
              "type":"pay",
              "money":-13,
              "date":"2022-09-24 16:36:42",
              "useFor":"dessert",
              "id":4
          },
          {
              "type":"income",
              "money":100000,
              "date":"2022-09-15 15:36:42",
              "useFor":"wage",
              "id":5
          }
      
      ]
        // 触发同步reducer方法
        dispatch(setBillList(res))
        
    }
}
// 导出异步方法
export {getBillList}
// 获取reducer方法
let billListReducer=billListSlice.reducer
// 导出reducer方法
export default billListReducer
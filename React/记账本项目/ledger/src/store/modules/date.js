import { createSlice, configureStore } from '@reduxjs/toolkit'
let now=new Date();
const dateSlice = createSlice({
// 状态数据名字
  name: 'date',
//初始值
  initialState: {
    value: now.toLocaleDateString()
  },
// 异步方法
  reducers: {
    // state:表示状态数据
    // action：表示调用该异步方法传过来的参数(action.payload)
    changeDate: (state,action) => {
      state.value =action.payload
    },
  },
  middleware:getDefaultMiddleware => getDefaultMiddleware({
    serializableCheck:false
 })
 
})

export const { changeDate } = dateSlice.actions

let dateReducer=dateSlice.reducer
export default dateReducer

// 第二步：在根store中合并子store

// 导入合并方法
import {configureStore} from "@reduxjs/toolkit"

// 导入子仓库
import counterReducer from "./modules/count"
import dateReducer from "./modules/date"
import billListReducer from "./modules/billList"

// 合并
const store =configureStore({
    reducer:{
        counter:counterReducer,
        date:dateReducer,
        billList:billListReducer
    }
})

// 导出仓库
export default store
// 第一步：准备数据
const obj={
    "pay":{
        // 餐饮
        "catering":["dine","drinks","dessert"],
        // 出行交通
        "traffic":["taxi","journey"],
        // 休闲娱乐
        "recreation":["movement","fallow","multimedia","holiday"],
        // 日常支出
        "daily":["clothes","bags","study","improvement","fitment"],
        // 其他支出
        "otherPay":["other"]

    },
    "income":{
        // 常规收入
        "income":["wage","overtime","bonus"],
        // 其他收入
        "otherIncome":["manageMoney","cashGift"]

    }

}

export default obj
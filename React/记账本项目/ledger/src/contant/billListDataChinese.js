// 第二步：准备汉化数据
const obj={
        // 餐饮
        "catering":"餐饮",
        "dine":"餐费",
        "drinks":"酒水饮料",
        "dessert":"甜点零食",
        // 出行交通
        "traffic":"出行交通",
        "taxi":"打车出租",
        "journey":"旅行票费",
        // 休闲娱乐
        "recreation":"休闲娱乐",
        "movement":"运动健身",
        "fallow":"休闲玩乐",
        "multimedia":"媒体影音",
        "hoilday":"旅游度假",
        // 日常支出
        "daily":"日常支出",
        "clothes":"衣服裤子",
        "bags":"鞋帽包包",
        "study":"知识学习",
        "improvement":"能力提升",
        "fitment":"家装布置",
        // 其他支出
        "otherPay":"其他支出",
        "other":"其他",
        // 常规收入
        "income":"常规收入",
        "wage":"工资",
        "overtime":"加班",
        "bonus":"奖金",
        // 其他收入
        "otherIncome":"其他收入",
        "manageMoney":"理财收入",
        "cashGift":"礼金收入"
    }



export default obj
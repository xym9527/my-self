// 路由配置文件
import Layout from "@/pages/layout";
import Month from "@/pages/month";
import New from "@/pages/new";
import Year from "@/pages/year";
import NotFound from "@/pages/notfound";
import { createBrowserRouter, Navigate } from "react-router-dom";
let router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "",
        element: <Navigate to="month"></Navigate>,//默认路由
      },
      {
        path: "month",
        element: <Month />,
      },
      {
        path: "year",
        element: <Year />,
      },
    ],
  },
  {
    path: "/new",
    element: <New />,
  },

  {
    path: "*",
    element: <NotFound></NotFound>,
  },
]);
export default router;

import { Outlet,useNavigate } from "react-router-dom";
import { Badge, TabBar } from "antd-mobile";
import "@/pages/layout/index.css"
import {
  AddCircleOutline,
  CalendarOutline,
  HandPayCircleOutline,
} from "antd-mobile-icons";

// 第三步：在layout组件中使用账单列表仓库

// 导入数据状态管理相关方法
import {useDispatch,useSelector} from "react-redux"
import { getBillList } from "@/store/modules/billList";
// 导入useEffect钩子函数
import { useEffect } from "react";

export default function Layout() {
  // 使用dispatch
  let dispatch=useDispatch();
  // 页面渲染后使用异步获取数据方法
  useEffect(()=>{
    dispatch(getBillList())
  },[]);

  // 查看是否获取到数据
  // let res=useSelector(state=>state.billList.value)
  // console.log("res",res);


  const tabs = [
    {
      key: "month",
      title: "月度收支",
      icon: <CalendarOutline />,
      badge: Badge.dot,
    },
    {
      key: "new",
      title: "记账",
      icon: <AddCircleOutline />,
    },
    {
      key: "year",
      title: "年度账单",
      icon: <HandPayCircleOutline />,
    },
  ];
  let navigate=useNavigate()
  const setRouteActive = (value) => {
    console.log(value);
    navigate("/"+value);
  }

  return (
    <div className="box">
      <div className="container">
        {/* 二级路由出口 */}

        <Outlet />
      </div>

      <div className="footer">
        <TabBar onChange={value => setRouteActive(value)}>
          {tabs.map((item) => (
            <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
          ))}
        </TabBar>
      </div>
    </div>
  );
}

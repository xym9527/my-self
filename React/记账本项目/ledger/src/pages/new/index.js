import Icon from "@/components/icons/icon";
import billListData from "@/contant/billListData";
import billListDataChinese from "@/contant/billListDataChinese";
import { useMemo, useState } from "react";
import { CalendarPicker, Input, Modal } from "antd-mobile";
import classNames from "classnames";
import axios from "axios";

export default function New() {
  // 声明变量用于控制渲染列表的类型
  let [billType, setBillType] = useState("pay");

  //用于控制日期选择框的显示与消失
  const [visible1, setVisible1] = useState(false);

  // 日期
  let now = new Date();
  const [checkDate, setCheckDate] = useState(now);

  // 日期时间处理："2023-02-12"
  let checkDateStr = useMemo(() => {
    let str = checkDate.getFullYear() + "-";
    if (checkDate.getMonth() + 1 < 10) {
      str += "0" + (checkDate.getMonth() + 1);
    } else {
      str += checkDate.getMonth() + 1;
    }
    str += "-" + checkDate.getDate();
    return str;
  }, [checkDate]);
  let nowStr = useMemo(() => {
    let str = now.getFullYear() + "-";
    if (now.getMonth() + 1 < 10) {
      str += "0" + (now.getMonth() + 1);
    } else {
      str += now.getMonth() + 1;
    }
    str += "-" + now.getDate();
    return str;
  }, [now]);

  //输入框的值
  const [value, setValue] = useState("");

  //用于保存支出或收入类型
  const [moneyType, setMoneyType] = useState("");

  return (
    <div>
      {/* 切换收入支出 */}
      <div>
        <button
          onClick={() => {
            setBillType("pay");
          }}
        >
          支出
        </button>
        <button
          onClick={() => {
            setBillType("income");
          }}
        >
          收入
        </button>
      </div>

      {/* 输入框 */}
      <div>
        {/* 日期选择框 */}
        <div
          onClick={() => {
            setVisible1(true);
          }}
        >
          <Icon type="block" />
          <CalendarPicker
            visible={visible1}
            selectionMode="single"
            defaultValue={checkDate}
            onClose={() => setVisible1(false)}
            onMaskClick={() => setVisible1(false)}
            max={now}
            // 点击确认
            onConfirm={(val) => {
              setCheckDate(val);
            }}
          />
          <span>{checkDateStr === nowStr ? "今天" : checkDateStr}</span>
        </div>

        {/* 输入框 */}
        <Input
          placeholder="0.00"
          value={value}
          onChange={(val) => {
            setValue(val);
          }}
        />
      </div>

      {/* 保存按钮 */}
      <button
        onClick={() => {
          // 获取账单信息
          // {
          //     "type":"pay",
          //     "money":-88,
          //     "date":"2022-10-24 10:12:40",
          //     "useFor":"longdistance",
          //     "id":2
          // }
          // 处理日期时间格式
          let timeStr =
            checkDateStr +
            " " +
            checkDate.getHours() +
            ":" +
            checkDate.getMinutes() +
            ":" +
            checkDate.getMinutes();

          if (value * 1 === 0 || value === "") {
            // 提示弹窗
            Modal.alert({
              content: "请输入金额！",
              onConfirm: () => {
                console.log("Confirmed");
              },
            });
            return;
          } else if (moneyType === "") {
            Modal.alert({
              content: "请选择金额使用类型！",
              onConfirm: () => {
                console.log("Confirmed");
              },
            });
            return;
          }

          let obj = {
            type: billType,
            // 处理金额:支出的金额为负数
            money: billType === "pay" && value * 1 > 0 ? "-" + value : value,
            date: timeStr,
            useFor: moneyType,
          };

          console.log(obj);

          // 调用接口上传账单信息
        //   axios
        //     .post({
        //       method: "post",
        //       url: "/user/12345",
        //       data: obj,
        //     })
        //     .then((res) => {
        //       // 清空数据
        //       setValue("");
        //       setMoneyType("");
        //     });
        }}
      >
        保存
      </button>

      {/* 支出或收入列表 */}
      <div>
        <div>
          {Object.keys(billListData[billType]).map((item, index) => (
            <div key={index}>
              <h3 className="title">{billListDataChinese[item]}</h3>
              <ul>
                {billListData[billType][item].map((item2, index2) => (
                  <li
                    key={index2}
                    id={item2}
                    onClick={(event) => {
                      if (event.target.localName === "li") {
                        // 修改类型
                        setMoneyType(event.target.id);

                        // 样式调整
                        let li = document.getElementsByTagName("li");
                        for (let i = 0; i < li.length; i++) {
                          li[i].style.backgroundColor = "white";
                        }
                        event.target.style.backgroundColor = "green";
                      }
                    }}
                  >
                    <Icon type={item2} />
                    <p>{billListDataChinese[item2]}</p>
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

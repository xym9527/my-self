import { DownOutline, UpOutline } from "antd-mobile-icons";
import "@/pages/month/index.css";
import { useEffect, useMemo, useState } from "react";
import { DatePicker, Toast } from "antd-mobile";
import classNames from "classnames";

// 第一步：导入状态管理相关方法
import { useDispatch, useSelector } from "react-redux";
import { decremented, incremented } from "@/store/modules/count";
import { changeDate } from "@/store/modules/date";

// 导入组件
import DayCheckCollect from "@/components/dayCheckCollect/dayCheckCollect";
import DayCheckList from "@/components/dayCheckList/dayCheckList";

export default function Month() {
  let now = new Date();

  const [visible, setVisible] = useState(false);

  const counter = useSelector((state) => state.counter.value);
  const date = useSelector((state) => state.date.value);
  console.log(date);
  let dateArr = useMemo(() => {
    let arr = date.split("/");
    return arr;
  }, [date]);

  const dispatch = useDispatch();

  // 第二步：获取仓库中账单列表数据
  let billList = useSelector((state) => state.billList.value);

  // 第三步：声明状态变量用于存储当前年份和月份得到分组账单列表
  let [currentMonthList, setCurrentMonthList] = useState([]);

  // 第四步：处理数据，把所有账单根据月份进行分组
  // useMemo:相当于vue中的计算属性
  // 第一个参数是回调函数：表示要对数据做的处理
  // 第二个参数是数组：表示第一个回调函数执行的依赖项，
  // 如果依赖性数据发生变化，回调函数就会再次执行
  let groupList = useMemo(() => {
    // 处理账单列表，相同年份和月份为一组
    // 返回值是一个对象：
    // obj={
    //   "2022-09":[{},{},{},...],
    //   "2022-10":[{},{},{},...],
    // }

    let obj = {};
    for (let i = 0; i < billList.length; i++) {
      // 获取每条账单的年份和月份："2022-10"
      let y = billList[i].date.slice(0, 7);
      // console.log(y);
      // 判断该年份和月份是否已经存在对象中，如果存在就插入该条数据，不存在就新增该属性
      if (y in obj) {
        obj[y].push(billList[i]);
      } else {
        obj[y] = [billList[i]];
      }
    }
    return obj;
  }, [billList]);

  // 第五步：获取当前年份和月份，并重新调整年份和月份的数据格式
  let dateStr = useMemo(() => {
    let str = dateArr[0] + "-";
    if (dateArr[1].length < 2) {
      str += "0" + dateArr[1];
    } else {
      str += dateArr[1];
    }
    // 根据当前年份和月份组成的属性名在账单分组数据中找到当前月份的数据，并修改状态数据
    if (groupList[str]) {
      setCurrentMonthList(groupList[str]);
    }
    return str;
  }, [dateArr, groupList]);

  // 第六步：统计当前月份的总支出、总收入、总结余
  let total = useMemo(() => {
    let pay = 0;
    let income = 0;
    let surplus = 0;
    for (let i = 0; i < currentMonthList.length; i++) {
      if (currentMonthList[i].type === "pay") {
        pay += currentMonthList[i].money * 1;
      } else {
        income += currentMonthList[i].money * 1;
      }
    }
    surplus = income + pay;

    return {
      pay,
      income,
      surplus,
    };
  }, [currentMonthList]);

  // 展示当月收支详情
  // 第一步：整理数据，把当月收支按日分类
  // obj={
  //   "2022-10-22":[{},{},{}],
  //   "2022-10-23":[{},{},{}]
  // }
  let dayGroupList = useMemo(() => {
    let obj = {};
    for (let i = 0; i < currentMonthList.length; i++) {
      // 获取每条账单的年份和月份："2022-10"
      let y = currentMonthList[i].date.slice(0, 10);
      // console.log(y);
      // 判断该年份和月份是否已经存在对象中，如果存在就插入该条数据，不存在就新增该属性
      if (y in obj) {
        obj[y].push(currentMonthList[i]);
      } else {
        obj[y] = [currentMonthList[i]];
      }
    }
    return obj;
  }, [currentMonthList]);

  // console.log("dayGroupList", dayGroupList);

  // 第三步：整理数据，把每天的收入、支出、结余汇总
  // obj={
  //   "2022-10-22":{
  //     pay:-88,
  //     income:200,
  //     surplus:122
  //   },
  //   "2022-10-21":{
  //     pay:-88,
  //     income:200,
  //     surplus:122
  //   }
  // }
  let dayTotal = useMemo(() => {
    let bigObj = {};
    for (let key in dayGroupList) {
      let obj = {
        pay: 0,
        income: 0,
        surplus: 0,
      };
      for (let i = 0; i < dayGroupList[key].length; i++) {
        if (dayGroupList[key][i].type === "pay") {
          obj.pay += dayGroupList[key][i].money;
        } else {
          obj.income += dayGroupList[key][i].money;
        }
      }
      obj.surplus = obj.income + obj.pay;
      bigObj[key] = obj;
    }
    return bigObj;
  }, [dayGroupList]);
  // console.log("dayTotal", dayTotal);

  const [flag,setFlag]=useState(true)

  return (
    <div>
      <button
        onClick={() => {
          dispatch(incremented());
        }}
      >
        +1
      </button>{" "}
      {counter} <button>-1</button>
      {/* 统计区域 */}
      <div className="total">
        <div
          onClick={() => {
            setVisible(true);
          }}
        >
          {dateArr[0]} | {dateArr[1]} 月账单
          {/* 使用classNames插件实现动态类名 */}
          <span className={classNames(visible ? "flag" : "")}>
            {" "}
            <DownOutline />
          </span>
          <span className={classNames(visible ? "" : "flag")}>
            {" "}
            <UpOutline />
          </span>
        </div>
        <div className="list">
          {/* 第七步：使用统计之后的结果 */}
          <div>
            <span>{total.pay}</span>
            <span>支出</span>
          </div>
          <div>
            <span>{total.income}</span>
            <span>收入</span>
          </div>
          <div>
            <span>{total.surplus}</span>
            <span>结余</span>
          </div>
        </div>

        <DatePicker
          title="时间选择"
          visible={visible}
          onClose={() => {
            setVisible(false);
          }}
          max={now}
          onConfirm={(val) => {
            dispatch(changeDate(val.toLocaleDateString()));
          }}
          precision="month"
        />

       
      </div>
       {/* 第二步：渲染月度收支详情 */}
       <div>
          <div className="box">

            {Object.keys(dayGroupList).map((item, index) => (
              <div key={index}>
                <p onClick={()=>{
                  setFlag(!flag);
                }}>{item}</p>
              {/* 使用组件 */}
                <DayCheckCollect data={dayTotal[item]}></DayCheckCollect>
                <div style={{"display":flag?"block":"none"}}>
                  <DayCheckList data={dayGroupList[item]}></DayCheckList>
                </div>
              </div>
            ))}
          </div>
        </div>
    </div>
  );
}

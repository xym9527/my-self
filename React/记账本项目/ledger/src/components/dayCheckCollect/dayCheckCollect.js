// 导入组件样式表
import "./dayCheckCollect.css"

export default function DayCheckCollect(props) {
    // props:接受父组件传过来的数据
    // console.log("props",props.data)
  return (
    <div className="allbox">
        <span>支出{props.data.pay}</span>
        <span>收入{props.data.income}</span>
        <span>{props.data.surplus}结余</span>
    </div>
  );
}

// 每日收支详情组件

// 导出icon图组件
import Icon from "../icons/icon";

export default function DayCheckList(props){
  // 收入、支出类型引文匹配中文
  let normalObj={
    "drinks":"酒水饮料",
    "longdistance":"出行",
    "bonus":"奖金",
    "dessert":"甜点零食",
    "wage":"工资"
  }

    return <ul>
    {props.data.map((item, index) => (
      <li key={index}>
        <span><Icon type={item.useFor}/>{normalObj[item.useFor]}</span>
        <span>{item.money}</span>
      </li>
    ))}
  </ul>
}
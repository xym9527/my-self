import drinks from "./icons/酒水饮料.png"
import longdistance from "./icons/出行.png"
import dessert from "./icons/甜点零食.png"
import wage from "./icons/工资.png"
import block from "./icons/时钟.png"

export default function Icon(props){
    let url="";
    if(props.type==="drinks"){
        url=drinks
    }else if(props.type==="longdistance"){
        url=longdistance
    }else if(props.type==="dessert"){
        url=dessert
    }else if(props.type==="wage"){
        url=wage
    }else if(props.type==="block"){
        url=block
    }
    return <img style={{"width":"20px","height":"20px"}} src={url} alt=""/>

}
import { createBrowserRouter, Link, Navigate } from "react-router-dom";
import Home from "../pages/home";
import About from "../pages/about";
import Count from "./../components/Count";
import Hello from "./../components/Hello";

const router = createBrowserRouter([
  {
    path: "/",
    title: "首页11",
    element: <Navigate to="/home"></Navigate>,
  },
  {
    path: "/home",
    title: "首页",
    element: <Home></Home>,
    children: [
      {
        path: "/home",
        element: <Navigate to={"/home/son"}></Navigate>,
      },
      {
        path: "/home/son",//绝对路由必须以父路由开头
        element: <Count></Count>,
      },
      {
        path: "hello",//相对路由，会自动拼接父路由
        // index:true,
        element: <Hello></Hello>,
      },
    ],
  },
  {
    path: "/about",
    title: "关于",
    element: <About></About>,
    children: [
      {
        path: "/about/son",
        element: <Count></Count>,
      },
    ],
  },
  {
    path: "*",
    title: "404",
    element: <div>404</div>,
  },
]);

export default router;

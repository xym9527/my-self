// 1.导入
import React from "react"
import "./comment.css"

// 2.创建react元素
class Comment extends React.Component{
    // 状态数据
    state={
        // 评论列表
        commentList:[
            {userName:"张三",word:"React是渲染界面的JavaScript库"},
            {userName:"李四",word:"JSX语法"},
            {userName:"王五",word:"创建组件的两种方式"},
        ],
        // 评论人
        userName:"",
        // 评论内容
        word:""
    }
    // 更新评论人和评论内容
    getData=(e)=>{
        let {name,value}=e.target
        this.setState({
            [name]:value
        })
    }
    // 渲染评论列表
    renderList(){
        if(this.state.commentList.length===0){
            return <div>暂无评论，快去评论吧~</div>
        }else{
            return <ul>
                {this.state.commentList.map((item,index)=><li key={index}>
                    <h2>评论人：{item.userName}</h2>
                    <p>评论内容：{item.word}</p>
                </li>)}
            </ul>
        }
    }
    // 增加评论
    addComment=()=>{
        let {commentList,userName,word}=this.state;
        if(!userName || !word) {
           alert("请输入评论人或者评论内容！");
           return;
        }
        let newList=[...commentList,{userName,word}];
        this.setState({
            commentList:newList,
            userName:"",
            word:''
        })
    }
    render() {
        let {userName,word}=this.state;
       return <div className="box">
        <input type="text" placeholder="请输入评论人" name="userName" value={userName} onChange={this.getData}/>
        <textarea placeholder="请输入评论内容" name="word" value={word} onChange={this.getData}></textarea>
        <button onClick={this.addComment}>发表评论</button>
        {this.renderList()}
       </div>
    }
}

export default Comment
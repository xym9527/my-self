import React from "react"
import PropTypes from 'prop-types'


class Mouse extends React.Component{
    state={
        x:0,
        y:0
    }
    changeMove=(e)=>{
        this.setState({
            x:e.clientX,
            y:e.clientY
        })
        console.log(this.state)

    }
    componentDidMount(){
        window.addEventListener("mousemove",this.changeMove)
    }
    render(){
        return this.props.children(this.state)
    }
    componentWillUnmount() {
        window.removeEventListener('mousemove', this.changeMove)
        }
}
Mouse.propTypes={
    children:PropTypes.func.isRequired
}

export default Mouse
import React from "react";


class Random extends React.PureComponent{
    state={
        obj:{
            num:0
        }
    }
    getRandom=()=>{
        let newObj={}
        newObj.num=Math.floor(Math.random()*(3-0+1))
        this.setState((state)=>{
            return {
                obj:newObj
            }
        },()=>{
            console.log("this.state:",this.state)
        })
    }
    // shouldComponentUpdate(nextProps,nextState){
    //     console.log("nextState：",nextState,"this.state:",this.state)
    //     if(nextState.num === this.state.num){
    //         return false
    //     }
    //     return true

    // }
    render(){
        console.log("render")
    let el=<div>
        {/* <H1 num={this.state.num}></H1> */}
        <h1>随机数：{this.state.obj.num}</h1>
        <button onClick={this.getRandom}>生成0-3之间的随机数</button>
        </div>
    console.log(el);
        return el
    }
}

// class H1 extends React.Component{
//     // shouldComponentUpdate(nextProps,nextState){
//     //     console.log("nextProps",nextProps,"this.props:",this.props)
//     //     return !(nextProps.num === this.props.num)

//     // }
//     render(){
//         console.log("render")

//         return <h1>随机数：{this.props.num}</h1>
//     }
// }

export default Random
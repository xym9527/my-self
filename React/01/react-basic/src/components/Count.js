// 1.导入
import React from "react"
import { useNavigate } from "react-router-dom";


class Count extends React.Component{
    state={
        count:0
    }
    changeCount=()=>{
        this.setState((state,props)=>{
            return {
                count:state.count+2
            }
        },()=>{
            // console.log("立即执行：",this.state.count)

        })
    }
    shouldComponentUpdate(nextProps,nextState){
        console.log("最新的state",nextState);
        if(nextState.count%3===0){
            return false
        }
        return true

    }
    gotoMouse=()=>{
        console.log(this)
        // const navigate = useNavigate();
        this.props.history.push('/count')
        // navigate("/mouse");
    }
    render(){
        console.log("render")
        return <div>
            <h1>计数器：{this.state.count}</h1>
            <button onClick={this.changeCount}>+1</button>
            <button onClick={this.gotoMouse}>点击跳转到Mouse页面</button>
        </div>
    }
}

export default Count
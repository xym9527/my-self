import React from "react"
import PropTypes from 'prop-types'

function widthMouse(Com){
    class Mouse extends React.Component{
        state={
            x:0,
            y:0
        }
        getMove=(e)=>{
            this.setState({
                x:e.clientX,
                y:e.clientY
            })
        }
        componentDidMount(){
            window.addEventListener("mousemove",this.getMove)
        }
        render(){
            return <Com {...this.state} {...this.props}></Com>
        }
    }
    return Mouse

}

export default widthMouse
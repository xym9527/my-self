// 1.导入
import React from "react";
// import ReactDOM  from "react-dom"
import { createRoot } from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
  BrowserRouter as Router,
} from "react-router-dom";

import router from "./router";
import App from "./App";

// 3.渲染元素
// ReactDOM.render(h1,document.getElementById("root"));
let container = document.getElementById("root");
let root = createRoot(container);
root.render(
  // <RouterProvider router={router} />
  <Router>
    <App></App>
  </Router>
);

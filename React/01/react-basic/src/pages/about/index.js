import {useParams,useSearchParams,Link,Outlet} from "react-router-dom"

export default function Home(){
    // let params=useParams();
    let [params]=useSearchParams();
    let id=params.get("id")
    return <div>
        <h1>关于---{id}</h1>
        <Link to="/about/son">son</Link>
        <Outlet/>
    </div>
}
import {Link ,useNavigate,Outlet} from "react-router-dom";


export default function Home(){
    const navigate = useNavigate();

    function gotoAbout(){
        navigate("/about?id=123&name=345")


    }
    return <div>
        <h1>首页</h1>
        {/* 生命式导航 */}
        <Link to="/about/666/888">about</Link>
        {/* 编程式导航 */}
        <button onClick={gotoAbout}>点击跳转</button>

        <Link to="/home/son">son</Link>
        <Link to="/home/hello">hello</Link>
        <Outlet/>
        
    </div>
}
// 路由的基本使用步骤：
// 1.安装路由：npm i react-router-dom

// 2.导入相关组件
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";

import router from "./router";
import "./App.css"


console.log(router);
function App() {
  return (
    // 3.在根组件的最外层使用Router组件，才能是内部使用路由

      <div className="App">
        <div className="nav">
          {/* 4.使用Link组件指定导航链接，相当于a标签 */}
          {router.routes.map((item,index)=>{
            return <Link key={index} to={item.path}>{item.title}</Link>
          })}
          
        </div>
        <div className="container">
          {/* 5.使用 Routes组件包裹所有的Route组件，这个是react-router-dom 6.0版本之后的写法
          之前的旧版不需要用Routes包裹*/}
          <Routes>
            {/* 6.Route展示路由跳转的组件 */}
            {
              router.routes.map((item,index)=>{
                return <Route key={index} path={item.path} element={item.element}>
                  { item.children ? item.children.map((item2,index2)=>{
                    return <Route key={index2} path={item2.path} element={item2.element}></Route>
                  }) : "" }

                </Route>
              })
            }
           
          </Routes>
        </div>
      </div>

  );
}

export default App;

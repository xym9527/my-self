// pages/list/list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 用于存放传过来的参数
    query:{},
    flag:false,
    listArr:[]

  },
  getData() {
    wx.showLoading({
      title: '数据加载中',
    });
    this.data.flag=true;
    let that=this;
    wx.request({
      url: 'https://opentdb.com/api.php?amount=10&category=23&difficulty=easy&type=multiple',
      success: function (res) {
        console.log("返回值：", res.data.results);
        for(var i=0;i<res.data.results.length;i++){
          that.data.listArr.push(res.data.results[i]);
        }
        that.setData({
          listArr:that.data.listArr,
        })
      },
      complete:function(){
        wx.hideLoading();
        that.setData({
          flag:false
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      query:options
    });
    this.getData();


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    wx.setNavigationBarTitle({
      title: this.data.query.title,
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    console.log("触底了",this.data.flag);
    if(!this.data.flag){
      this.getData();
    }

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
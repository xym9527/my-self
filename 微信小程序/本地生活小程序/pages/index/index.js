// index.js

Page({
  data: {
    imgList: [],
    menuList:["美食","洗浴足疗","结婚啦","卡拉OK","找工作","辅导班","汽车保养","租房","装修"]
  },
  getImgs() {
    var that = this;
    for (var i = 0; i <= 3; i++) {
      wx.request({
        url: 'https://api.thecatapi.com/v1/images/search?size=full',
        method: "GET",
        success: function (res) {
          that.data.imgList.push(res.data[0].url);
          that.setData({
            imgList: that.data.imgList
          })

        }
      })
    };


  },
  onLoad() {
    this.getImgs();

  }

})
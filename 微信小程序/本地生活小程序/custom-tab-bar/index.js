// custom-tab-bar/index.js
import {storeBindingsBehavior} from "mobx-miniprogram-bindings";
import {store} from "./../store/store"
Component({
  // 实现自动绑定
  behaviors:[storeBindingsBehavior],
  // 绑定的具体内容
  storeBindings:{
    store,//要绑定的仓库
    // 指定要绑定的字段
    fields:{
      // 组件中使用的变量名=>指向store仓库中的变量名
      activeIndex:"tabBarActiveIndex",
      info:"newsInfo"
    },
    // 指定要绑定的方法
    actions:{
      updateActiveIndex:"updateActiveIndex"
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    active: 0,
    list: [{
      "pagePath": "pages/index/index",
      "text": "首页",
      "iconPath": "/icons/home.png",
      "selectedIconPath": "/icons/home_active.png"
    },{
      "pagePath": "pages/news/news",
      "text": "消息",
      "iconPath": "/icons/news.png",
      "selectedIconPath": "/icons/news_active.png",
      "info":2
    },{
      "pagePath": "pages/conection/conection",
      "text": "联系",
      "iconPath": "/icons/connection.png",
      "selectedIconPath": "/icons/connection_active.png"
    }]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onChange(event) {
      // event.detail 的值为当前选中项的索引
      // this.setData({ active: event.detail });
      // console.log("事件对象：",event)
      let index=event.detail;//当前点击导航栏的下标
      // 跳转tabBar页面
      wx.switchTab({
        url: "/"+this.data.list[index].pagePath,
      });
      // 调用store中的方法，修改activeIndex
      this.updateActiveIndex(index);
    }

  }
})
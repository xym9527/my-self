import {observable,action} from "mobx-miniprogram";

export const store=observable({
  // 数据字段
  tabBarActiveIndex:0,
  newsInfo:3,
  // 计算属性

  // actions方法
  // 修改点击导航栏下标
  updateActiveIndex:action(function(index){
    this.tabBarActiveIndex=index;
  }),
  // 增加消息数量
  addNewsInfo:action(function(){
    this.newsInfo=this.newsInfo*1+1;
  }),
  // 减少消息数量
  subNewsInfo:action(function(){
    if(this.newsInfo<=1){
      this.newsInfo="";
    }else{
      this.newsInfo-=1;
    }
  }),

})
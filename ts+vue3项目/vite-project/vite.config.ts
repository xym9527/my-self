import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    createSvgIconsPlugin({
      // 指定要缓存的文件夹 icons文件夹里面全是svg图标
      iconDirs: [resolve(process.cwd(), 'src/assets/icons')],
      // 指定symbolId格式  使用时候<use :xlink:href="'#'+name" :fill="color"></use> 记得加#
      symbolId: '[name]',
    }),
  ],
  resolve: {
    // 路径别名
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
      '#': fileURLToPath(new URL('./node_modules', import.meta.url)),
    },
    // 省略文件名后缀
    extensions: ['.ts', '.scss', '.json', '.vue', '.js']
  },
  css: {
    preprocessorOptions: {
      scss: {
        // 配置多个全局scss变量
        additionalData: `@import '@/assets/styles/variables.scss';`,
        // 注意：最后的分号一定要加，否则会报错
      }
    }
  },
  // 本地运行配置，及反向代理配置
  server: {
    host:"0.0.0.0",
    cors: true, // 默认启用并允许任何源
    // open: true, // 在服务器启动时自动在浏览器中打开应用程序
    port: 4000,
    //反向代理配置，注意rewrite写法，开始没看文档在这里踩了坑
    proxy: {// 本地开发环境通过代理实现跨域，生产环境使用 nginx 转发
      '/api': {
        target: 'http://localhost:3000',
        // 通过代理接口访问实际地址。这里是实际访问的地址。vue会通过代理服务器来代理请求
        changeOrigin: true,
        ws: true,  // 允许websocket代理
        rewrite: (path) => {
          // console.log("xxxxxxxxxxx",path);
          // console.log(path.replace(/^\/api/, ''));
          return path.replace(/^\/api/, '')
        }, // 将api替换为空
        // bypass(req, res, options) {
        //   // const proxyUrl = new URL( options.rewrite(req.url) || '',(options.target) as string ) ?.href || "";
        //   // console.log("真实的请求地址：",proxyUrl);
        //   const proxyUrl = options.target + options.rewrite(req.url);
        //   console.log(`真实请求的完整地址proxyUrl: ${proxyUrl}`);
        // },
      }
    },
    headers:{
      size:100000000
    }
  }

})

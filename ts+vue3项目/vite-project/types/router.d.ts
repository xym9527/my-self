/* 路由全局自定义约束 */

//引入路由自带的约束
import type {RouteRecordRaw} from 'vue-router'

//自定义的配置
export interface RouteItem {
  // path:是否需要展示该路由,是否渲染该路由入口
  hidden?: boolean
  // meta:元数据
  // 可以作用判断用户是否已登陆
  // 可以通过meta值，展示面包屑
  meta?: {
    title?: string
    path?: string
  }
  children?: MyRoutesTy
}

//合并配置生成自己的配置
export type MyRouteTy = RouteRecordRaw & RouteItem

//合并配置的数组
export type MyRoutesTy = Array<MyRouteTy>
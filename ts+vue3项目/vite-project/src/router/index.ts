import { createRouter, createWebHistory } from 'vue-router'
//引入自己的约束
import type { MyRoutesTy } from '~/router';

// 静态引入框架
import LayoutVue from '@/layout/Layout.vue';

const routes: MyRoutesTy = [
  // 路由重定向
  {
    path: "/",
    redirect: "/login"
  },
  // 登录
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/index.vue')
  },
  // 首页
  {
    path: '/home',
    name: 'home',
    component: LayoutVue,
    meta: { title: "后台首页", path: "/home" },
    children: [
      {
        path: "/home",
        component: () => import('@/views/home/index.vue')
      }
    ]
  },
  // 用户管理
  {
    path: '/user',
    name: 'user',
    component: LayoutVue,
    meta: { title: "用户管理", path: "/user" },
    redirect: "/user/list",
    children: [
      {
        path: "/user/list",
        meta: { title: "用户列表", path: "/user/list" },
        component: () => import('@/views/user/user-list.vue')
      },
      {
        path: "/user/add",
        meta: { title: "添加用户", path: "/user/add" },
        component: () => import('@/views/user/user-add.vue')
      },
      {
        path: "/user/mine",
        meta: { title: "个人中心", path: "/user/mine" },
        component: () => import('@/views/user/user-mine.vue')
      }
    ]
  },
  // 文章管理
  {
    path: '/article',
    name: 'article',
    component: LayoutVue,
    meta: { title: "文章管理", path: "/article" },
    redirect: "/article/list",
    children: [
      {
        path: "/article/list",
        meta: { title: "文章列表", path: "/article/list" },
        component: () => import('@/views/article/article-list.vue')
      },
      {
        path: "/article/edit",
        meta: { title: "文章编辑", path: "/article/edit" },
        component: () => import('@/views/article/article-edit.vue')
      }
    ]
  },
  // 表格管理
  {
    path: '/excel',
    name: 'excel',
    component: LayoutVue,
    meta: { title: "表格管理", path: "/excel" },
    redirect: "/excel/import",
    children: [
      {
        path: "/excel/import",
        meta: { title: "表格导入", path: "/excel/import" },
        component: () => import('@/views/excel/excel-import.vue')
      },
      {
        path: "/excel/export",
        meta: { title: "表格导出", path: "/excel/export" },
        component: () => import('@/views/excel/excel-export.vue')
      }
    ]
  },
  // 404
  {
    path: "/404",
    component: () => import('@/views/404/index.vue')
  },
  // 404重定向
  {
    // 之前vue2是path:"*"，表示任意路径错误都到404页面
    // 现在vue3是path:"/:pathMatch(.*)",可以百度vue-router 404
    path: "/:pathMatch(.*)",
    redirect: "/404"
  }


];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

//导航守卫
//使用router.beforeEach注册一个全局前置守卫，判断用户是否登录
router.beforeEach((to,from,next)=>{
  if(to.path === '/login'){
    next();
  }else{
    let token = localStorage.getItem('token');
    if( token === null || token === ''){
      next('/login')
    }else{
      next();
    }
  }
});

export default router

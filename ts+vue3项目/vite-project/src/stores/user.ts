/* 用户模块 */
import { defineStore } from 'pinia'
import { ref } from 'vue'

// 导入封装好的axios
import request from "@/network/request";

// 调接口传过来的参数
interface LoginT {
    userName: string,
    passWord: string
}

// 服务端返回值的类型约束
interface ResT{
  code:number,
  data:object,
  token?:string
}

export const useUserStore = defineStore('useStore', () => {
    //token 
    const token = ref<string>('') //保证数据刷新不丢失

    //用户信息
    const userInfo = ref({})
    
    //发送登录的请求
    const P_login = (data: LoginT) => {
        return new Promise(async resolve => {
          let res:any = await request({
            url:"/user/login",
            method:"get",
            params:data
          })
          console.log("res",res);
          //判断
          if (res.code  === 200) {
            //1.数据存pinia
            userInfo.value=res.data[0];
            // 同时把用户信息存在本地，防止刷新后数据丢失
            // 主要是为了进入系统后，欢迎用户，需要获取用户名
            sessionStorage.setItem("user",JSON.stringify(res.data[0]));
            console.log(userInfo);
            
            //2.把token数据存本地
            token.value=res.token;
            localStorage.setItem("token",token.value);
            //切换状态到已成功,调用resolve函数
            resolve(userInfo);
          }else{
            alert("用户名或密码错误！");
          }
        })
      }

    //外面需要使用的暴露
    return {
        P_login,
        userInfo,
    }
})

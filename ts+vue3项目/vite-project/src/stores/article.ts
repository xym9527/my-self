import { ref, computed, reactive, shallowRef } from 'vue'

import { createPinia,defineStore } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

interface ArticleT {
    [propName: string]: string;
}
export const useArticleStore = defineStore('article', () => {
    const articleInfor = shallowRef<ArticleT>({});
    let articleFn = (data: ArticleT) => {
        articleInfor.value = data;
    }
    return { articleInfor, articleFn }
},{persist: {
    paths: ['articleInfor'],
    storage: sessionStorage,
    key: 'my_articleInfor'
  }})

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);
export default pinia;
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', () => {
  const collapseFlag = ref<boolean>(false);
  const toggleFlag = ()=>{
    collapseFlag.value=!collapseFlag.value;
  }

  return { collapseFlag,toggleFlag }
})

//hooks函数 以use开头的函数

import { onMounted, onUnmounted, ref } from 'vue'

// 导入pinia
import { storeToRefs } from 'pinia'
import { useCounterStore } from "@/stores/counter";


//导出一个hooks函数
export function useResize() {
  // 解构属性为响应式数据
  let $useCounterStore = useCounterStore()
  let { collapseFlag } = storeToRefs($useCounterStore);
  // 注意：仓库实例化必须在函数中，否则会报错

  //处理宽度
  const $resize = () => {
    if (innerWidth >= 800) {
      collapseFlag.value = false;
    } else {
      collapseFlag.value = true;
    }
  }
  $resize()

  onMounted(() => {
    window.addEventListener('resize', $resize)
  })

  onUnmounted(() => {
    window.removeEventListener('resize', $resize)
  })

  //把外面用的 暴露出去 没有返回值可以不用return
  // return {
  //   collapseFlag,
  // }
}
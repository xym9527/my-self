import "@/assets/styles/index";
import { createApp } from 'vue'

import { createPinia } from 'pinia'
import piniaPersist from 'pinia-plugin-persistedstate'
 
const pinia = createPinia()
pinia.use(piniaPersist)



import App from './App.vue'
import router from './router'
import 'virtual:svg-icons-register'


const app = createApp(App)

//引入组件
import SvgIcon from '@/components/SvgIcon.vue'

//注册全局组件
app.component('SvgIcon', SvgIcon)

// 导入element-plus插件
import ElementPlus from 'element-plus'
// 导入样式
import 'element-plus/dist/index.css'
// 中文配置
import zhCn from 'element-plus/es/locale/lang/zh-cn'
// 全局注册插件
app.use(ElementPlus, {size: 'small', zIndex: 3000, locale: zhCn})

// 引入echarts
import echarts from "@/utils/echarts";
// echarts 挂载到 Vue实例中
// 注意：app.config.globalProperties 和 app.provide('$echarts', echarts) 二选一即可
// Vue.prototype.$echarts = echarts; // vue2的挂载方式

// app.config.globalProperties.$echarts = echarts; 
// vue3的挂载方式（一个用于注册能够被应用内所有组件实例访问到的全局属性的对象。）

app.provide('$echarts', echarts); 
// vue3采用provide, inject方式使用


app.use(createPinia())
app.use(router)

app.mount('#app')

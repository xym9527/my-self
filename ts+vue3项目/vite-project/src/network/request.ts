import axios from "axios";
import type { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
// import { useRouter } from "vue-router";
// 因为在vue3中useRouter，useStore要放在setup中引入，我们在封装axios文件中不能直接引入。
// 所以改为如下的方式引入全局路由对象
import $router from "@/router/index"


export default function request(config: AxiosRequestConfig) {
    // 实例化全局路由必须写到函数里面（还是会报警告）
    // let $router = useRouter();

    // 创建axios实例
    const axiosInstance: AxiosInstance = axios.create({
        baseURL: "/api",
        timeout: 5000,

    });

    // axios的拦截器
    // （1）请求拦截器的作用
    // =>修改config中的一些不符合服务器要求的信息
    // =>在页面中展示发送网络请求时，等待的请求的图标
    // =>检测某些网络请求（比如登录（token）），必须携带一些特殊的信息
    axiosInstance.interceptors.request.use(config => {
        // console.log("来到了请求成功的拦截中", config);
        // console.log("来到了请求成功的拦截中");
        // 每次请求都加上token
        config.headers.Authorization = "Bearer " + localStorage.getItem('token');
        config.headers["Content-Type"]="application/x-www-form-urlencoded";
        return config;
        //一定要返回config，否则就意味着发送请求失败
        // 因为在这里把请求拦截下来了
    }, err => {
        // console.log("xxxx",err);
        // console.log("来到了请求失败的拦截中");
        return err;
    })

    // (2)响应拦截器的作用
    // =>响应的成功拦截中，主要是对数据进行过滤
    // =>响应的失败拦截中，可以根据status判断报错的错误码，跳转到不同的错误提示页面。
    axiosInstance.interceptors.response.use((response: AxiosResponse) => {
        // console.log(response);
        // console.log("来到了响应成功的拦截中");
        return response.data;
        // 对数据进行过滤，因为data才是真正服务器返回的数据，
        // 其他的内容都是axios这个框架自己加的，不需要
    }, err => {
        // console.log(err);
        // console.log("来到了响应失败的拦截中");
        if (err && err.response) {
            switch (err.response.status) {
                case 400:
                    err.message = "请求错误";
                    break;
                case 401:
                    // token过期
                    err.message = "未授权访问";
                    alert("登录过期，请重新登录！");
                    // 回到登录页面
                    $router.replace("/login");
                    break;
            }

        }
        return err;

    });

    return axiosInstance(config);
}
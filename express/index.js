const express = require('express')
const router = express.Router();
const app = express()
const port = 3000

const http = require('http');
http.createServer({
    maxHeaderSize: 10 * 1024 * 1024
})

// 连接数据库
var mysql = require('mysql')
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'studentdb'
})
// token的使用步骤
// 1.生成token模块：npm install jsonwebtoken
// 2.验证token模块：npm install  express-jwt

// 3.引入生成token模块
let jwt = require("jsonwebtoken");
// 4.定义密钥
let secret = "xyz";

// 5.引入验证token文件
let jwtAuth = require("./jwtAuth");
// 6.接口进行token验证
app.use(jwtAuth);

app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    // res.header("Access-Control-Allow-Headers", "X-Requested-With");
    // res.header('Access-Control-Allow-Headers', 'Content-Type');
    // res.header('Access-Control-Allow-Headers', 'mytoken');
    next();
});

// 登录
app.get('/user/login', (req, res) => {
    // 设置响应头，实现跨域，第二个参数是指定可以跨域的地址，*表示所有
    // res.setHeader('Access-Control-Allow-Origin', '*');
    console.log(req.query); //获取传过来的参数对象
    let sql = `SELECT * FROM users WHERE account="${req.query.userName}" AND password="${req.query.passWord}"`;
    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 找到用户，可以登录
            if (result.length == 1) {
                // 7.根据用户的账号、密码、自定义密钥字符串，生成一个token字符串
                let token = jwt.sign(req.query, secret, {
                    // 过期时间：单位秒
                    expiresIn: 60 * 60
                })
                // 返回数据
                res.send({
                    code: 200,
                    data: result,
                    token,
                });
            } else { //没有找到用户
                res.send({
                    code: 400,
                    data: {}
                })

            }

        }
    })

})

// 用户分页
app.get('/user/list', (req, res) => {
    // 设置响应头，实现跨域，第二个参数是指定可以跨域的地址，*表示所有
    // res.setHeader('Access-Control-Allow-Origin', '*');
    // console.log(req.query); //获取传过来的参数对象
    let sql = `SELECT * FROM users LIMIT ${req.query.startIndex},${req.query.pageSize}`;
    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回数据
            res.send({
                code: 200,
                data: result,
            });
        }
    })

})

// 用户总数
app.get('/user/allNumber', (req, res) => {
    // 设置响应头，实现跨域，第二个参数是指定可以跨域的地址，*表示所有
    // res.setHeader('Access-Control-Allow-Origin', '*');
    // console.log(req.query); //获取传过来的参数对象
    let sql = `SELECT COUNT(*) FROM ${req.query.excelName}`;
    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回数据
            res.send({
                code: 200,
                data: result,
            });
        }
    })

})

// 文章分页
app.get('/user/article', (req, res) => {
    // 设置响应头，实现跨域，第二个参数是指定可以跨域的地址，*表示所有
    // res.setHeader('Access-Control-Allow-Origin', '*');
    // console.log(req.query); //获取传过来的参数对象
    let sql = `SELECT * FROM article LIMIT ${req.query.startIndex},${req.query.pageSize}`;
    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回数据
            res.send({
                code: 200,
                data: result,
            });
        }
    })

})
// app.get('/', (req, res) => {
//     // 设置响应头，实现跨域，第二个参数是指定可以跨域的地址，*表示所有
//     // res.setHeader('Access-Control-Allow-Origin', '*');
//     // 返回数据
//     res.send("hello world");
// })


// 修改用户信息接口
app.get('/user/update', (req, res) => {
    let sql = `UPDATE users set account="${req.query.username}",role='${req.query.role}' WHERE id=${req.query.id} ;`;
    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回数据
            res.send({
                code: 200,
                data: result,
            });
        }
    })

})
// 修改文章信息接口
app.post('/article/update', (req, res) => {

    res.setHeader('Access-Control-Allow-Origin', '*');
    console.log("参数：", req.query);
    console.log("参数：", req.body);
    console.log("参数：", req.params);
    console.log("res参数：", res);
    let sql = `UPDATE article set content="${req.query.content}",title='${req.query.title}' WHERE id=${req.query.id} ;`;
    connection.query(sql, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            // 返回数据
            res.send({
                code: 200,
                data: result,
            });
        }
    })

})

// 处理post传参的模块
const bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
// 上传表格接口
app.post('/excel/update', (req, res) => {

    console.log("参数：", req.body);
    console.log("参数：", req.body["0[学号]"]);
    // let sql = `UPDATE article set content="${req.query.content}",title='${req.query.title}' WHERE id=${req.query.id} ;`;
    // connection.query(sql, (err, result) => {
    //     if (err) {
    //         console.log(err);
    //     } else {
    //         // 返回数据
    //         res.send({
    //             code: 200,
    //             data: result,
    //         });
    //     }
    // })

})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
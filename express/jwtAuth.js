// 引入验证token模块
const {expressjwt}=require("express-jwt");
// 自定义密钥
let secret="xyz";

const jwtAuth=expressjwt({
    secret,
    algorithms:["HS256"]//解密规则
}).unless({
    // 除了登录接口之外，都需要验证token
    path:["/user/login","/article/update"]
})
module.exports=jwtAuth;
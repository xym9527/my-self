//hooks函数 以use开头的函数

import {onMounted, onUnmounted, ref} from 'vue'
//导出一个hooks函数
export function useResize() {
  const w = ref(100)
  //处理宽度
  const $resize = () => {
    const BODYW = document.body.clientWidth
    w.value = BODYW - 150
    console.log('65666', 65666)
  }
  $resize()

  onMounted(() => {
    window.addEventListener('resize', $resize)
  })

  onUnmounted(() => {
    window.removeEventListener('resize', $resize)
  })

  //把外面用的 暴露出去
  return {
    w,
  }
}
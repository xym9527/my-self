import { createApp } from 'vue'
// 1.导入创建App根对象的方法：具体源码可以按住ctrl键，再点击变量跳转到详细源码
import App from './App.vue'
//导入路由
import router from "./router/index"
// 导入全局样式:需要安装sass插件(npm i sass --save)
import "@/assets/css/index";

// createApp(App).mount('#app')
createApp(App).use(router).mount('#app');

// 2.相当于如下写法：
// let app=createApp(App);//创建app根对象
// // 使用路由一定要在挂载前
// app.use(router);
// app.mount("#app");//把根元素挂载到根对象中
// 注意：vue2中是替代index.html里面的id="app"的元素，
//      vue3中是把内容插入到id="app"的元素里面，
//      因此在vue3的项目中要注意id="app"的元素的样式


// 1.createRouter 创建路由工厂函数
// createWebHashHistory 选用历史模式 vue3 历史模式刷新不报错
// createWebHistory 或者选用hash模式
import { createRouter, createWebHashHistory } from "vue-router";

// 4.静态引入路由组件(懒加载路由组件)
let Setup = () => import('@/views/setup/index');
let Ref = () => import('@/views/ref/index');
let Child = () => import('@/views/ref/Child');
let Life = () => import('@/views/life/index');
let Father = () => import('@/views/fatherChild/Father');
let Child2 = () => import('@/views/childFather/Father');
let Hooks = () => import('@/views/hooks');
let Shop = () => import('@/views/shop');

// 配置路由地址和页面级别组件的一一对应关系
export const routes = [
  {
    path:"/",//根路径
    redirect:"/setup" 
    // redirect是重定向
  },
  {
    path: '/setup',
    component: Setup
  },
  {
    path: '/ref',
    component: Ref
  },
  {
    path: '/child',
    component: Child
  },
  {
    path: '/life',
    component: Life
  },
  {
    path: '/father',
    component: Father
  },
  {
    path: '/child2',
    component: Child2
  },
  {
    path: '/hooks',
    component: Hooks
  },
  {
    path: '/shop',
    component: Shop
  },

]

// 2.创建路由
const router = createRouter({
  history: createWebHashHistory(), //hash模式
  routes //3.导入路由配置
})

// 5.导出配置好的路由
export default router
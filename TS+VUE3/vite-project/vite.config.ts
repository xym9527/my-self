import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// 1.path resolve方法获取本地绝对路径
import { resolve } from "path";
// 注意：如果报错：找不到模块“path”或其相应的类型声明。
// 解决方案：npm install --save-dev @types/node

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    // 2.别名处理
    alias: {
      '@': resolve(__dirname, 'src'), //别名 @相当于src 文件夹目录
    },
    // 3.省略文件名后缀
    extensions: ['.js', '.ts', '.jsx', '.tsx', '.json', '.vue', '.mjs','.scss']
  },
})
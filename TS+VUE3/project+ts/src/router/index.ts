import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/ref',
      name: 'ref',
      component: () => import('../views/ref.vue')
    },
    {
      path: '/reactive',
      name: 'reactive',
      component: () => import('../views/reactive.vue')
    },
    {
      path: '/watch',
      name: 'watch',
      component: () => import('../views/watch.vue')
    }
  ]
})

export default router
